# MySQL Schema Difference Finder #
======

A PHP script to compare MySQL database schemas. More information available on [the blog post](http://joef.co.uk/blog/2009/07/php-script-to-compare-mysql-database-schemas/).

### Setup Guide ###

* Rename `config.php.dist` to `config.php`.
* Replace database dummy credentials with real one
* in `config.php`, there is a `tablePattern` parameter, It's added to filter tablename from huge list of tables (if any) under one database, it works same like `LIKE` operator in MySQL.